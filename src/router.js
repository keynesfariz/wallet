import Vue from 'vue'
import Router from 'vue-router'
import firebase from '@/firebase'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      component: () => import('./views/Login.vue'),
      meta: { guest: true },
    },
    {
      path: '/register',
      component: () => import('./views/Register.vue'),
      meta: { guest: true },
    },
    {
      path: '/',
      component: () => import('./views/Home.vue'),
    },
    {
      path: '/transactions',
      component: () => import('./views/Transaction.vue'),
    },
    {
      path: '/transactions/create',
      component: () => import('./views/TransactionCreate.vue'),
    },
    {
      path: '/wallets',
      component: () => import('./views/Wallet.vue'),
    },
    {
      path: '/wallets/create',
      component: () => import('./views/WalletCreate.vue'),
    },
    {
      path: '/profile',
      component: () => import('./views/Profile.vue'),
    },
    {
      path: '/profile/edit',
      component: () => import('./views/ProfileEdit.vue'),
    },

  ]
})

router.beforeEach((to, from, next) => {
    const auth = !(to.meta && to.meta.guest)
    firebase.auth().onAuthStateChanged(user => {
      if (user && !auth) {
        next('/')
      } else if (!user && auth) {
        next('/login')
      } else {
        next()
      }
  })
})

export default router