import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import vueHeadful from 'vue-headful'

import'./filters'
import '@/assets/css/tailwind.css'
import '@fortawesome/fontawesome-free/css/solid.min.css'
import '@fortawesome/fontawesome-free/css/fontawesome.min.css'

Vue.config.productionTip = false
Vue.component('vue-headful', vueHeadful)

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
