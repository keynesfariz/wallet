import firebase from 'firebase/app'
import 'firebase/auth'

const firebaseConfig = {
  apiKey: 'AIzaSyBXv5gR1h6h8KWEOLnp-GKqsE38ZUmynB4',
  authDomain: 'money-2be9f.firebaseapp.com',
  databaseURL: 'https://money-2be9f.firebaseio.com',
  projectId: 'money-2be9f',
  storageBucket: 'money-2be9f.appspot.com',
  messagingSenderId: '565889336371',
  appId: '1:565889336371:web:c237dd9510290579'
}

export default firebase.initializeApp(firebaseConfig)