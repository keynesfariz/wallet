import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import firebase from '@/firebase'

Vue.use(Vuex)

const baseUrl = 'http://wallet.keynesfariz.com/backend/public/'

const state = {
  status: {},
  wallets: [],
  transactions: [],
  categories: [],
}

const getters = {
  status: state => state.status,
  wallets: state => state.wallets,
  transactions: state => state.transactions,
  categories: state => state.categories,
}

const actions = {
  // Read ...
  async fetchCategories({ commit }) {
    const response = await axios.post(baseUrl + 'categories', { user_id: firebase.auth().currentUser.uid })
    commit('setCategories', response.data)
  },
  async fetchStatus({ commit }) {
    const response = await axios.post(baseUrl + 'summary', { user_id: firebase.auth().currentUser.uid })
    commit('setStatus', response.data)
  },
  async fetchWallets({ commit }) {
    const response = await axios.post(baseUrl + 'wallets', { user_id: firebase.auth().currentUser.uid })
    commit('setWallets', response.data)
  },
  async fetchTransactions({ commit }) {
    const response = await axios.post(baseUrl + 'transactions', { user_id: firebase.auth().currentUser.uid })
    commit('setTransactions', response.data)
  },

  // Create...
  async storeWallet({ commit }, wallet) {
    const response = await axios.post(baseUrl + 'wallets/store', {
      user_id: firebase.auth().currentUser.uid,
      name: wallet.name,
      type: wallet.type,
    })
    commit('addWallet', response.data)
  },
  async storeTransaction({ commit }, trans) {
    // console.log(trans)
    // console.log(trans.wallet)
    const response = await axios.post(baseUrl + 'transactions/store', {
      user_id: firebase.auth().currentUser.uid,
      wallet_id: trans.wallet.id,
      category_id: trans.category.id,
      date: trans.date,
      type: trans.type,
      amount: trans.amount,
      note: trans.note,
    })
    commit('addTransaction', response.data)
  },

}
const mutations = {
  setStatus: (state, status) => state.status = status,
  setWallets: (state, wallets) => state.wallets = wallets,
  setCategories: (state, categories) => state.categories = categories,
  setTransactions: (state, transactions) => state.transactions = transactions,
  
  addWallet: (state, wallet) => state.wallets.unshift(wallet),
  addTransaction: (state, trans) => state.transactions.unshift(trans)
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})