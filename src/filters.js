import Vue from 'vue'
import moment from 'moment'

const currency = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2
})

Vue.filter('ago', value => {
    if (value) return moment(value).fromNow()
})

Vue.filter('date', value => {
    if (value) return moment(value).format('MMM D, YYYY')
})

Vue.filter('money', value => {
    return currency.format(value)
})

Vue.filter('monthYear', value => {
    if (value) return moment(value).format('M/DD')
    else return 'X/XX'
})

Vue.filter('secure', value => {
    if (value) {
        value = moment(value).format('x')
        value = value.toString()
        return '**** **** **** ' + value.substring(4, 8)
    } else {
        return '**** **** **** ****'
    }
})

